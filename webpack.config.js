const path = require('path');

module.exports = {
  entry: './src/character.js',
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'dist')
  }
};