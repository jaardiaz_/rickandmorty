
window.onload = function () {
    let page = 1;
    getCharacter(page);

    document.getElementById('next').onclick = function () {
        if (page < 25) {
            page = page + 1;
            deleteCharacter();
            getCharacter(page);
        }
    }

    document.getElementById('prev').onclick = function () {
        if (page > 1) {
            page = page - 1;
            deleteCharacter();
            getCharacter(page);
        }
    }

}

// Se pintan los personajes en el body
function printCharacter(data, page) {

    const numPage = document.getElementById('numPage');
    const divHome = document.getElementById('divHome');

    let divAllCards = document.createElement('div');
    divAllCards.className = 'ui link cards four column stackable grid';
    divAllCards.id = 'divAllCards';

    divHome.appendChild(divAllCards);

    numPage.textContent = "Page #" + page;

    data.results.forEach(element => {

        let divCard = document.createElement('div');
        let divCardImage = document.createElement('div');
        let imgCard = document.createElement('img');
        let divContent = document.createElement('div');
        let divContentHeader = document.createElement('div');
        let divContentDescription = document.createElement('div');

        divCard.className = 'card';
        divCard.id = 'divCard';
        divAllCards.appendChild(divCard);

        divCardImage.className = 'image';
        imgCard.setAttribute("src", element.image);
        divCardImage.appendChild(imgCard);

        divCard.appendChild(divCardImage);
        divContent.className = 'content';

        divContentHeader.className = 'header';
        divContentHeader.textContent = element.name;
        divContent.appendChild(divContentHeader);

        divContentDescription.className = 'description';

        let genderIcon = element.gender == 'Male' ? '<i class="mars icon blue"></i>' : '<i class="venus icon pink"></i>';
        let speciesIcon = element.species == 'Human' ? '<i class="child icon"></i>' : '<i class="rocket icon"></i>';
        let statusIcon = element.status == 'Alive' ? '<i class="heart icon red"></i>' : '<i class="heart icon"></i>';

        let description = speciesIcon + element.species + '<br />' + genderIcon + element.gender + '<br />' + statusIcon + element.status;

        divContentDescription.innerHTML = description;
        divContent.appendChild(divContentDescription);
        divCard.appendChild(divContent);

        let btnEpisodes = document.createElement('div');
        btnEpisodes.className = 'ui bottom attached button';
        btnEpisodes.id = 'btnOpenModal' + element.id;
        btnEpisodes.setAttribute("idCard", element.id);
        btnEpisodes.innerHTML = '<i class="desktop icon"></i>Episodes';
        divCard.appendChild(btnEpisodes);

        getBtnOpenModal(element.id);

    });
}

function getBtnOpenModal(idCard) {

    document.getElementById('btnOpenModal' + idCard).onclick = async function () {
            await printModal(idCard);
            $('.ui.longer.modal').modal({
                closable  : false,
                onApprove : function() {
                    deleteModal();
                }
              }).modal('show');
    }
}

// Se pintan los episodios por cada personaje en un modal
async function printModal(idCard) {


    const divHome = document.getElementById('divHome');

    let divModal = document.createElement('div');
    let divModalHeader = document.createElement('div');
    let divModalContent = document.createElement('div');
    let divModalImage = document.createElement('div');
    let divModalDescription = document.createElement('div');
    let divModalControl = document.createElement('div');

    let data = await getAllInfo(idCard);

    divModal.className = 'ui longer modal';
    divModal.id = 'divModal';

    divModalHeader.className = 'header';
    divModalHeader.textContent = data.character.name;

    divModalContent.className = 'scrolling image content';

    divModalImage.className= 'image content';
    divModalImage.innerHTML = '<div class="ui medium image"><img src="'+data.character.image+'"</div>';
    divModalContent.appendChild(divModalImage);

    divModalDescription.className = 'description';
    data.episodes.forEach( element => {
        let divModalDescriptionContent = document.createElement('div');
        let htmlEpisode = '<div class="ui divided selection list"><a class="item">'+
        element.codEpisode +' - ' + element.name + ' - ' + element.date+'</a></div>';
        divModalDescriptionContent.innerHTML = htmlEpisode;
        divModalDescription.appendChild(divModalDescriptionContent);
    });

    divModalControl.className = 'actions';

    divModalControl.innerHTML = '<div class="ui blue ok inverted button"><i class="checkmark icon"></i>OK</div>';

    divModalContent.appendChild(divModalDescription);
    divModal.appendChild(divModalHeader);
    divModal.appendChild(divModalContent);
    divModal.appendChild(divModalControl);    

    divHome.appendChild(divModal);

}



// Aquí se obtienen todos los personajes por pagina.
async function getCharacter(page) {

    let url = 'https://rickandmortyapi.com/api/character?page=' + page;

    await fetch(url)
        .then(response => response.json())
        .then(responseData => {
            let data = responseData != null ? responseData : 'No existen personajes';
            printCharacter(data, page);
        })
        .catch(err => {
            alert('Hubo un error mientras se cargaron los personajes', err);
        });
}

// Aquí se construye un objeto con la informacion necesaria a pintar.
async function getAllInfo(idCard){

    let infoCharacter = await getInfoCharacter(idCard);

    let episodes = new Array();
    for (const url of infoCharacter.urlEpisode) {
        let episode =  await getInfoEpisode(url);
        episodes.push(episode);
      }

    let allInfo = {
        "character" : infoCharacter,
        "episodes" : episodes
        };

    return allInfo;
}

// Aquí se obtiene el nombre y la imagen por personaje.
async function getInfoCharacter(idCard){

    let url = 'https://rickandmortyapi.com/api/character/' + idCard;

    let data = await fetch(url).then(response => response.json());

    let infoEpisode = {
        "name" : data.name, 
        "image" : data.image,
        "urlEpisode" : data.episode
    };

    return infoEpisode;
}

// Aquí se obtiene la info de cada episodio por personaje.
async function getInfoEpisode(url){

    let data = await fetch(url).then(response => response.json());

    let infoEpisode = {
        "codEpisode" : data.episode,
        "name" : data.name, 
        "date" : data.air_date
    };

    return infoEpisode;
}

function deleteCharacter() {
    document.getElementById('divAllCards').remove();
}

function deleteModal() {
    setTimeout(
        function() {
            document.getElementById('divModal').remove();
        },1000);
}