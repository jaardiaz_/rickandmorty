# README #

### What is this repository for? ###
Repositorio con proyecto desarrollado en javascript con webpack, semantic (css framework).
Se relizan peticiones GET a endpoint expuestos por https://rickandmortyapi.com/ y se listan de forma dinamica.

Endpoint: 
https://rickandmortyapi.com/api/character
https://rickandmortyapi.com/api/episode

### How do I get set up? ###

* Se instala Node js.
* Se instala webpack.
* Se configura package.json (cmd de ejecucion dev: npm run dev, pdn: npm run produccion).

### Url public ###

https://rickandmortyy.now.sh/